﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CS481_HW03_Tabbed
{
    public partial class MainPage : TabbedPage
    {
        int count = 0;

        public MainPage()
        {
            InitializeComponent();
        }

        //Entry for user to change his/her name
        void OnEntryTextChanged(object sender, TextChangedEventArgs e)
        {
            string oldText = e.OldTextValue;
            string newText = e.NewTextValue;
            name.Text = newText;

        }

        //Sends complete entry from user to be viewed
        void OnEntryCompleted(object sender, EventArgs e)
        {
            string text = ((Entry)sender).Text;
        }

        //Entry for user to change his/her name
        void OnEntryTextChanged2(object sender, TextChangedEventArgs e)
        {
            string oldText = e.OldTextValue;
            string newText = e.NewTextValue;
            address.Text = newText;

        }
        //Sends complete entry from user to be viewed
        void OnEntryCompleted2(object sender, EventArgs e)
        {
            string text = ((Entry)sender).Text;
        }

        

        //Displays Alert upon opening of app 
        //Dipslays the number of books user has when page 1 opens
        void OnAppearing(Object sender, System.EventArgs e)
        {
            DisplayAlert("Hello", "You have opened the Libary. Feel free to have a look around", "OK!");
            
        }
        

        void OnDisappearing(Object sender, System.EventArgs e)
        {

                DisplayAlert("BYE!", "You are leaving the Libary. Enjoy your books!", "OK");

        }



        void Add(Object sender, System.EventArgs e)
        {
            if (count == 2)
            {
                DisplayAlert(" Maybe", "just this once", "Checkout 3");
                count++;
            }
            else if (count < 3)
            {
                
                count++;
            }
            else { DisplayAlert("Sorry", "You can't checkout anymore books.", "OK"); }
            
            books.Text = count.ToString();
            book.Text = count.ToString();
        }

        void Remove(Object sender, System.EventArgs e)
        {
            if(count > 0)
                --count;
            books.Text = count.ToString();
            book.Text = count.ToString();
        }


    }
}
